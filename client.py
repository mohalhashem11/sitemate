import requests
from flask import Flask, request

app = Flask(__name__)

SERVER_ENDPOINT = 'http://127.0.0.1:3001'

@app.route('/issue/create', methods=['POST'])
def create_issue():
    try:
        req = requests.post(f'{SERVER_ENDPOINT}/issue/create', json=request.get_json())
        return req.json()
    except Exception as e:
        result = {
            'message': f'Error: {e}'
        }
        return result, 500

@app.route('/issue/<issue_id>/read', methods=['GET'])
def read_issue(issue_id):
    try:
        req = requests.get(f'{SERVER_ENDPOINT}/issue/{issue_id}/read')
        return req.json()
    except Exception as e:
        result = {
            'message': f'Error: {e}'
        }
        return result, 500

@app.route('/issue/<issue_id>/update', methods=['POST'])
def update_issue(issue_id):
    try:
        req = requests.post(f'{SERVER_ENDPOINT}/issue/{issue_id}/update', json=request.get_json())
        return req.json()
    except Exception as e:
        result = {
            'message': f'Error: {e}'
        }
        return result, 500

@app.route('/issue/<issue_id>/delete', methods=['DELETE'])
def delete_issue(issue_id):
    try:
        req = requests.delete(f'{SERVER_ENDPOINT}/issue/{issue_id}/delete')
        return req.json()
    except Exception as e:
        result = {
            'message': f'Error: {e}'
        }
        return result, 500

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True, port= 3002)
