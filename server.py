from flask import Flask, request
import json

app = Flask(__name__)

FILE_NAME = 'data.json'

def read_issues_file():
    try:
        file = open(FILE_NAME, 'r+')
        data = json.load(file)
        file.close()
        return data
    except Exception as _:
        return {}

def write_to_file(data):
    file = open(FILE_NAME, 'w')
    json.dump(data, file, indent=4, sort_keys=True)
    file.close()

def add_new_issue(issue_id, data):
    issues = read_issues_file()
    issues[issue_id] = data
    write_to_file(issues)

@app.route('/issue/create', methods=['POST'])
def create_issue():
    try:
        data = request.get_json()
        add_new_issue(data['id'], data)
        return data
    except Exception as e:
        result = {
            'message': f'Error: {e}'
        }
        return result, 500

@app.route('/issue/<issue_id>/read', methods=['GET'])
def read_issue(issue_id):
    try:
        issues = read_issues_file()
        if issue_id in issues:
            return issues[issue_id]
        else:
            result = {
                'message': f'No issue with id {issue_id} is found!'
            }
            return result, 201

    except Exception as e:
        result = {
            'message': f'Error: {e}'
        }
        return result, 500

@app.route('/issue/<issue_id>/update', methods=['POST'])
def update_issue(issue_id):
    try:
        issues = read_issues_file()
        if issue_id in issues:
            data = request.get_json()
            data['id'] = issue_id
            add_new_issue(issue_id, data)
            return data
        else:
            result = {
                'message': f'No issue with id {issue_id} is found!'
            }
            return result, 201
    except Exception as e:
        result = {
            'message': f'Error: {e}'
        }
        return result, 500

@app.route('/issue/<issue_id>/delete', methods=['DELETE'])
def delete_issue(issue_id):
    try:
        issues = read_issues_file()

        if issue_id in issues:
            temp = issues[issue_id]
            del issues[issue_id]
            write_to_file(issues)
            return temp
        else:
            result = {
                'message': f'No issue with id {issue_id} is found!'
            }
            return result, 201
    except Exception as e:
        result = {
            'message': f'Error: {e}'
        }
        return result, 500

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True, port=3001)